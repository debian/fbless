% FBLESS(1) fbless Manual
% Con Radchenko
% 2016-08-29
# NAME

fbless -- console fb2 reader

# SYNOPSIS

fbless { book.fb2 | book.fb2.{zip | gz | bz2} }

# DESCRIPTION

Fbless creates full-size ncurses window, with content of book
displayed and only piece of user interface is statusbar at
bottom. Interactrion with program is invisible, in vim style,
using following default keys:

Up,k
:	scroll one line up

Down,j
:	scroll one line down

PgUp
:	scroll one page up

Space,PgDown
:	scroll one page down

Home,g
:	scroll to the beginning of file

End
:	scroll to the end of file

5,G
:	move to position in file (you will be interactively asked
	for percents value in range from 0 to 100, where 0 is
	the beginning of file and 100 is the end of file)

Tab
:	move pointer on the next link

Enter,Right
:	follow link under pointer

Left, h
:	return back

Backspace, l
:	re-follow last link

a
:	toggle auto-scroll mode

f
:	enable fifo-style scrolling

+,-
:	increase/decrease autoscroll interval (1sec granuality)

/
:	search for regular expression

n
:	go to the next search result

s
:	toggle statusbar

q
:	quit

**fbless** saves position in file on quit, and restores it next time
you open same file, unless you move it in the meantime.

# FILES

~/.fbless_save
:	file to store last viewed positions in books

~/.fblessrc, $XDG\_BASE\_DIR/fbless/fblessrc
:	configuration file, creating and editing it allows customize colors,
	keybindings and some other options. Default one in installed under
	*/usr/share/doc/fbless*

# NOTES

This manual page was written by Dmitry Bogatov for Debian project,
but can be used by others under terms of GPLv2+ license.
